# Natural Language Processing Notebook

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.ub.uni-bielefeld.de%2Fpjentsch%2Fnlp_notebook/master)

This repository includes all files needed to run the Natural Language Processing demo Notebook with Binder. Click on the badge above to launch an interactive Binder environment.
